import {NestFactory,} from '@nestjs/core';
import {AppModule} from './app.module';
import {NestExpressApplication} from "@nestjs/platform-express";
import {DocumentBuilder, SwaggerModule} from "@nestjs/swagger";
import { utilities as nestWinstonModuleUtilities, WinstonModule } from 'nest-winston';
import * as helmet from 'helmet';
import * as compression from 'compression';
import * as winston from "winston";
import {HttpExceptionFilter} from "./common/exception_filters/http-exception.filter";

function initSwagger(app) {
    const options = new DocumentBuilder()
        .setTitle('Flip Api Task')
        .setDescription('The cart API description')
        .setVersion('1.0')
        .addTag('cart')
        .build();

    const document = SwaggerModule.createDocument(app, options);
    SwaggerModule.setup('docs', app, document);
}

async function bootstrap() {
    const app = await NestFactory.create<NestExpressApplication>(AppModule,
        {
            logger: WinstonModule.createLogger({
                format: winston.format.json(),
                transports: [
                    new winston.transports.Console({
                        format: winston.format.combine(
                            winston.format.timestamp(),
                        ),
                    }),
                    new winston.transports.File({ filename: __dirname + '/error.log', level: 'error' })
                ],
                exitOnError: false
            }),
        }
    );

    app.enableCors();
    app.use(helmet());
    app.use(compression());

    app.useGlobalFilters(new HttpExceptionFilter());

    initSwagger(app);

    await app.listen(process.env.APP_PORT);
    console.log(`Application is running on: ${await app.getUrl()}`);
}

bootstrap();