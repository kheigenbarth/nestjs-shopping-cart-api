import * as mongoose from 'mongoose';
import {ProductSchema} from "./product.schema";
import {CreateProductDto} from "../dto/create-product.dto";
import {ProductNotFoundException} from "../exceptions/product-not-found.exception";

export const CartSchema = new mongoose.Schema({
    products: {type: [ProductSchema], default: []},
});

CartSchema.methods = {

    addProduct(product: CreateProductDto): void {
        this.products.push(product);
    },

    removeProductById(id: string): void {
        const product = this.products.id(id);

        if (!product) {
            throw new ProductNotFoundException(id)
        }

        product.remove();
    },
};