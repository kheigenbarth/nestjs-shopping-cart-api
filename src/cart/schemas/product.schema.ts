import * as mongoose from 'mongoose';

export const ProductSchema = new mongoose.Schema({
    name: String,
    price: Number,
    quantity: Number,
    currency: String,
    description: String,
});

export const ProductCurrencyList = [
    'CAD', 'HKD', 'ISK', 'PHP',
    'DKK', 'HUF', 'CZK', 'AUD',
    'RON', 'SEK', 'IDR', 'INR',
    'BRL', 'RUB', 'HRK', 'JPY',
    'THB', 'CHF', 'SGD', 'PLN',
    'BGN', 'TRY', 'CNY', 'NOK',
    'NZD', 'ZAR', 'USD', 'MXN',
    'ILS', 'GBP', 'KRW', 'MYR',
    'EUR'
];
