import {Module} from '@nestjs/common';
import {CartController} from './cart.controller';
import {CartService} from './cart.service';
import {ExchangeRatesModule} from "../exchange-rates/exchange-rates.module";
import {MongooseModule} from "@nestjs/mongoose";
import {CartSchema} from "./schemas/cart.schema";

@Module({
    imports: [ExchangeRatesModule, MongooseModule.forFeature([{name: 'Cart', schema: CartSchema}])],
    controllers: [CartController],
    providers: [CartService],
})
export class CartModule {
}