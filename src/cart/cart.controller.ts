import {
    Controller,
    HttpCode,
    Get,
    Post,
    Delete,
    Param,
    Query,
    Body,
    UsePipes,
    ValidationPipe,
} from '@nestjs/common';
import {CartService} from "./cart.service";
import {CreateCartDto} from "./dto/create-cart.dto";
import {Cart} from "./interfaces/cart.interface";
import {CreateProductDto} from "./dto/create-product.dto";
import {ParamsCartDto} from "./dto/params-cart.dto";
import {ExchangeRatesService} from "../exchange-rates/exchange-rates.service";
import {QueryCartDto} from "./dto/query-cart.dto";
import {CartCheckout} from "./interfaces/cart-checkout.interface";
import {ApiBody, ApiOperation, ApiParam, ApiQuery, ApiResponse, ApiTags} from "@nestjs/swagger";
import {ProductCurrencyList} from "./schemas/product.schema";

@ApiTags('cart')
@Controller('cart')
@UsePipes(ValidationPipe)
export class CartController {

    constructor(
        private readonly cartService: CartService,
        private readonly exchangeRatesService: ExchangeRatesService
    ) {
    }

    @Post()
    @HttpCode(201)
    @ApiOperation({
        summary: 'Create cart.',
        description: 'Create cart.'
    })
    @ApiResponse({ status: 201, description: 'The cart has been successfully created.'})
    @ApiBody({type: CreateCartDto, required: false})
    create(@Body() cart: CreateCartDto): Promise<Cart> {
        return this.cartService.create(cart)
    }

    @Post('product/:cartId')
    @HttpCode(201)
    @ApiOperation({
        summary: 'Add product to cart.',
        description: 'Add product to cart.'
    })
    @ApiResponse({ status: 201, description: 'Product has been successfully added to cart.'})
    @ApiResponse({ status: 404, description: 'Cart not found.'})
    @ApiBody({type: CreateProductDto, description: "Product object"})
    @ApiParam({name: 'cartId', type: String, required: true, description: "Cart id"})
    async addProduct(@Body() product: CreateProductDto, @Param() params: ParamsCartDto): Promise<Cart> {
        const cart = await this.cartService.getById(params.cartId);
        cart.addProduct(product);

        return cart.save();
    }

    @Delete(':cartId')
    @HttpCode(200)
    @ApiOperation({
        summary: 'Delete cart.',
        description: 'Delete cart.'
    })
    @ApiResponse({ status: 200, description: 'Cart has been successfully deleted.'})
    @ApiResponse({ status: 404, description: 'Cart not found.'})
    @ApiParam({name: 'cartId', type: String, required: true, description: "Cart id"})
    async deleteCart(@Param() params: ParamsCartDto): Promise<Cart> {
        return this.cartService.deleteById(params.cartId);
    }

    @Delete(':cartId/product/:productId')
    @HttpCode(200)
    @ApiOperation({
        summary: 'Delete product.',
        description: 'Delete product from cart.'
    })
    @ApiResponse({ status: 200, description: 'Product has been successfully deleted from cart.'})
    @ApiResponse({ status: 404, description: 'Cart not found.'})
    @ApiParam({name: 'cartId', type: String, required: true, description: "Cart id"})
    @ApiParam({name: 'productId', type: String, required: true, description: "Product id"})
    async deleteProduct(@Param() params: ParamsCartDto): Promise<Cart> {
        const cart = await this.cartService.getById(params.cartId);
        cart.removeProductById(params.productId);

        return cart.save();
    }

    @Get(':cartId/checkout')
    @HttpCode(200)
    @ApiOperation({
        summary: 'Checkout cart.',
        description: 'Checkout cart.'
    })
    @ApiResponse({ status: 200, description: 'Cart checkout.'})
    @ApiResponse({ status: 404, description: 'Cart not found.'})
    @ApiParam({name: 'cartId', type: String, required: true, description: "Cart id"})
    @ApiQuery({ name: 'currency', enum: ProductCurrencyList })
    async checkout(@Param() params: ParamsCartDto, @Query() query: QueryCartDto): Promise<CartCheckout> {
        const cart: Cart = await this.cartService.getById(params.cartId);
        const total: number = await this.exchangeRatesService.calculateCartTotalPriceByCurrency(cart, query.currency);

        return {cart, total};
    }

    @Get()
    @HttpCode(200)
    @ApiOperation({
        summary: 'Get all carts.',
        description: 'Get all carts.'
    })
    @ApiResponse({ status: 200, description: 'Cart list.'})
    @ApiResponse({ status: 404, description: 'Cart not found.'})
    async getAll(): Promise<Cart[]> {
        return this.cartService.getAll();
    }

    @Get(':cartId')
    @HttpCode(200)
    @ApiOperation({
        summary: 'Get cart.',
        description: 'Get cart.'
    })
    @ApiResponse({ status: 200, description: 'Cart list.'})
    @ApiResponse({ status: 404, description: 'Cart not found.'})
    @ApiParam({name: 'cartId', type: String, required: true, description: "Cart id"})
    getById( @Param() params: ParamsCartDto): Promise<Cart> {
        return this.cartService.getById(params.cartId);
    }
}
