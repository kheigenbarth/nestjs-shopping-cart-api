import {IsIn, IsNotEmpty, IsOptional, IsPositive, IsString} from 'class-validator';
import {ProductCurrencyList} from "../schemas/product.schema";
import {ApiProperty} from "@nestjs/swagger";
import {IsCurrencyValue} from "../../common/decorators/IsCurrencyValue.decorator";

export class CreateProductDto {

    @IsNotEmpty()
    @IsString()
    @ApiProperty({
        description: 'Product name',
        required: true
    })
    name: string;

    @IsNotEmpty()
    @IsCurrencyValue({message: 'price must be a positive number with 0 or 2 decimal points'})
    @ApiProperty({
        description: 'Product price',
        required: true,
        default: 15.50,
        examples: [15.50, 10.30, 22.55, 14, 55]
    })
    price: number;

    @IsNotEmpty()
    @IsPositive()
    @ApiProperty({
        description: 'Product quantity',
        required: true,
        minimum: 1,
        default: 1
    })
    quantity: number;

    @IsNotEmpty()
    @IsString()
    @IsIn(ProductCurrencyList)
    @ApiProperty({
        description: 'Product currency',
        required: true,
        default: "PLN",
        enum: ProductCurrencyList
    })
    currency: string;

    @IsOptional()
    @IsString()
    @ApiProperty({
        description: 'Product description',
        required: false,
    })
    description?: string;
}