import {IsArray, IsOptional, ValidateNested} from 'class-validator';
import {CreateProductDto} from "./create-product.dto";
import {Product} from "../interfaces/product.interface";
import {ApiProperty} from "@nestjs/swagger";

export class CreateCartDto {
    @IsOptional()
    @IsArray()
    @ValidateNested({ each: true })
    @ApiProperty({
        required: false,
        type: [CreateProductDto],
        example: [
            {
                name: 'product name',
                description: 'product desc',
                quantity: 1,
                price: 2.50,
                currency: 'EUR'
            },
        ]
    })
    products?: Product[] | CreateProductDto[]
}