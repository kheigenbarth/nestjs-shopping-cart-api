import {IsIn, IsNotEmpty} from 'class-validator';
import {ProductCurrencyList} from "../schemas/product.schema";

export class QueryCartDto {
    @IsNotEmpty()
    @IsIn(ProductCurrencyList)
    currency: string;
}