import {IsMongoId, IsOptional} from 'class-validator';

export class ParamsCartDto {
    @IsMongoId()
    @IsOptional()
    cartId?: string;

    @IsMongoId()
    @IsOptional()
    productId?: string
}