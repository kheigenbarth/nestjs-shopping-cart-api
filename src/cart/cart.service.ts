import {Injectable} from '@nestjs/common';
import {CreateCartDto} from "./dto/create-cart.dto";
import {Cart} from "./interfaces/cart.interface";
import {Model} from "mongoose";
import {InjectModel} from "@nestjs/mongoose";
import {CartNotFoundException} from "./exceptions/cart-not-found.exception";

@Injectable()
export class CartService {

    constructor(@InjectModel('Cart') private cartModel: Model<Cart>) {}

    create(createCartDto: CreateCartDto): Promise<Cart> {
        const cart = new this.cartModel(createCartDto);
        return cart.save();
    }

    async deleteById(id: string): Promise<Cart> {
        const cart = await this.cartModel.findByIdAndDelete(id).exec();
        if(!cart) throw new CartNotFoundException(id);

        return cart;
    }

    getAll(): Promise<Cart[]> {
        return this.cartModel.find().exec();
    }

    async getById(id: string): Promise<Cart> {
        const cart  = await this.cartModel.findById(id).exec();
        if(!cart) throw new CartNotFoundException(id);

        return cart;
    }
}
