import { Test, TestingModule } from '@nestjs/testing';
import { CartService } from './cart.service';
import MongoDatabaseMock from "../common/mocks/db-mock"
import {MongooseModule} from "@nestjs/mongoose";
import {CartSchema} from "./schemas/cart.schema";

describe('CartService', () => {
    let service: CartService;

    beforeAll(async () => {

        await MongoDatabaseMock.connect();

        const module: TestingModule = await Test.createTestingModule({
            imports: [
                MongooseModule.forRoot(await MongoDatabaseMock.getUri(),
                    {
                        useNewUrlParser: true, useUnifiedTopology: true
                    }),
                MongooseModule.forFeature([{name: 'Cart', schema: CartSchema}]),
            ],
            providers: [CartService],
        }).compile();

        service = module.get<CartService>(CartService);
    });

    afterAll(async () => {
        await MongoDatabaseMock.closeDb();
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });

    it('should create cart', async () => {
        const cart = await service.create({});
        const carts = await service.getAll();

        expect(carts).toHaveLength(1);
        expect(carts[0].toJSON()).toEqual(cart.toJSON());
    });

    it('should delete cart', async () => {
        let carts = await service.getAll();
        const cartsLengthBeforeDeletion = carts.length;

        const cart = await service.getById(carts[0]._id);
        await service.deleteById(cart._id);

        carts = await service.getAll();
        expect(carts).toHaveLength(cartsLengthBeforeDeletion - 1);
    });
});