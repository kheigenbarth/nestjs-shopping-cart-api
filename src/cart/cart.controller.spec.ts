import {Test, TestingModule} from '@nestjs/testing';
import {CartController} from './cart.controller';
import MongoDatabaseMock from "../common/mocks/db-mock"
import {CartService} from "./cart.service";
import {MongooseModule} from "@nestjs/mongoose";
import {CartSchema} from "./schemas/cart.schema";
import {Cart} from "./interfaces/cart.interface";
import {ExchangeRatesService} from "../exchange-rates/exchange-rates.service";
import {HttpService} from "@nestjs/common";
import {CartCheckout} from "./interfaces/cart-checkout.interface";
import {Product} from "./interfaces/product.interface";
import {ProductNotFoundException} from "./exceptions/product-not-found.exception";
import {CartNotFoundException} from "./exceptions/cart-not-found.exception";

describe('Cart Controller', () => {
    let controller: CartController;
    let exchangeRatesService: ExchangeRatesService;

    beforeAll(async () => {

        await MongoDatabaseMock.connect();

        const module: TestingModule = await Test.createTestingModule({
            imports: [
                MongooseModule.forRoot(await MongoDatabaseMock.getUri(),
                    {
                        useNewUrlParser: true, useUnifiedTopology: true
                    }),
                MongooseModule.forFeature([{name: 'Cart', schema: CartSchema}]),
            ],
            controllers: [CartController],
            providers: [
                CartService,
                {
                    provide: ExchangeRatesService,
                    useValue: {
                        prepareCheckoutForDemandCurrency: jest.fn(),
                    }
                },
                {
                    provide: HttpService,
                    useClass: jest.fn()
                },
            ],
        }).compile();

        controller = module.get<CartController>(CartController);
        exchangeRatesService = module.get<ExchangeRatesService>(ExchangeRatesService);
    });

    afterAll(async () => {
        await MongoDatabaseMock.closeDb();
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });

    it('should create cart', async () => {
        const cart: Cart = await controller.create({});
        const carts: Cart[] = await controller.getAll();

        expect(carts).toHaveLength(1);
        expect(carts[0].toJSON()).toEqual(cart.toJSON());
    });

    it('should create cart with products', async () => {

        const product1 = {"currency": "PLN", "description": "desc1", "name": "1", "price": 13.50, "quantity": 3};
        const product2 = {"currency": "PLN", "description": "desc2", "name": "2", "price": 88.30, "quantity": 7};
        const product3 = {"currency": "PLN", "description": "desc3", "name": "3", "price": 44.55, "quantity": 5};

        const cart: Cart = await controller.create({products: [product1, product2, product3]});

        expect(cart.products).toHaveLength(3);
    });

    it('should add product to the cart', async () => {
        const carts: Cart[] = await controller.getAll();
        let cart = carts[0];

        const product1 = {"currency": "PLN", "description": "desc1", "name": "1", "price": 13.50, "quantity": 3};
        const product2 = {"currency": "PLN", "description": "desc2", "name": "2", "price": 88.30, "quantity": 7};
        const product3 = {"currency": "PLN", "description": "desc3", "name": "3", "price": 44.55, "quantity": 5};

        await controller.addProduct(product1, {cartId: cart._id});
        await controller.addProduct(product2, {cartId: cart._id});
        await controller.addProduct(product3, {cartId: cart._id});

        cart = await controller.getById({cartId: cart._id});

        expect(cart.products).toHaveLength(3);
    });

    it('should delete product from the cart', async () => {
        const carts: Cart[] = await controller.getAll();
        let cart: Cart = carts[0];

        const productLengthBeforeDeletion = cart.products.length;

        const productToDelete: Product = cart.products[0];
        await controller.deleteProduct( {cartId: cart._id, productId: productToDelete._id});

        cart = await controller.getById({cartId: cart._id});

        expect(cart.products).toHaveLength(productLengthBeforeDeletion - 1);
    });

    it('should throw ProductNotFoundException', async () => {
        const carts: Cart[] = await controller.getAll();
        const  cart: Cart = carts[0];

        const notExistingMongoId = '5f01e173a7fa782c9f21f533';

        controller.deleteProduct( {cartId: cart._id, productId: notExistingMongoId}).catch(e => {
            expect(e).toBeInstanceOf(ProductNotFoundException);
        });
    });

    it('should delete cart', async () => {
        let carts: Cart[] = await controller.getAll();
        const cart = carts[0];

        const cartsLengthBeforeDeletion = carts.length;
        await controller.deleteCart( {cartId: cart._id});

        carts = await controller.getAll();

        expect(carts).toHaveLength(cartsLengthBeforeDeletion - 1);
    });

    it('should throw CartNotFoundException', () => {
        const notExistingMongoId = '5efcb690ef097d007e8f468c';

        controller.deleteCart({cartId: notExistingMongoId}).catch(err => {
            expect(err).toBeInstanceOf(CartNotFoundException);
        });

        controller.getById({cartId: notExistingMongoId}).catch(err => {
            expect(err).toBeInstanceOf(CartNotFoundException);
        });
    });

    it('should checkout the cart', async () => {
        const cart: Cart = await controller.create({});

        const product1 = {"currency": "PLN", "description": "desc1", "name": "1", "price": 50.00, "quantity": 3};
        const product2 = {"currency": "EUR", "description": "desc2", "name": "2", "price": 25.50, "quantity": 5};
        const product3 = {"currency": "GBP", "description": "desc3", "name": "3", "price": 44.55, "quantity": 7};

        await controller.addProduct(product1, {cartId: cart._id});
        await controller.addProduct(product2, {cartId: cart._id});
        await controller.addProduct(product3, {cartId: cart._id});

        const mockedSumOfCart = 50.00;

        exchangeRatesService.calculateCartTotalPriceByCurrency = jest.fn().mockReturnValue(mockedSumOfCart);

        const cartCheckout: CartCheckout = await controller.checkout({cartId: cart._id}, {currency: "PLN"});

        expect(cartCheckout.total).toEqual(mockedSumOfCart);
    });
});
