import {Cart} from "./cart.interface";

export interface CartCheckout {
    cart: Cart
    total: number
}