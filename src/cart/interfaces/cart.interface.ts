import {Document} from 'mongoose';
import {Product} from "./product.interface";
import {CreateProductDto} from "../dto/create-product.dto";

export interface Cart extends Document {
    products: Product[]
    total: number
    removeProductById: (productId: string) => void
    addProduct: (product: CreateProductDto) => void
}