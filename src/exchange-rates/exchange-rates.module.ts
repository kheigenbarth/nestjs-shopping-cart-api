import {HttpModule, Module} from '@nestjs/common';
import {ExchangeRatesService} from "./exchange-rates.service";

@Module({
    imports: [HttpModule,],
    controllers: [],
    providers: [ExchangeRatesService],
    exports: [ExchangeRatesService]
})
export class ExchangeRatesModule {}
