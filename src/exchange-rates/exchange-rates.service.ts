import {CacheKey, CacheTTL, HttpService, Inject, Injectable} from '@nestjs/common';
import {Cart} from "../cart/interfaces/cart.interface";
import {Product} from "../cart/interfaces/product.interface";
import {CreateProductDto} from "../cart/dto/create-product.dto";
import {CreateCartDto} from "../cart/dto/create-cart.dto";

@Injectable()
export class ExchangeRatesService {
    constructor(
        // @Inject('REDIS_SERVICE') private client: ClientProxy,
        private readonly httpService: HttpService,
    ) {
    }

    private readonly baseExchangeCurrency: string = 'EUR';
    private rates: Array<{ key: string, value: string }>;

    @CacheKey('rates')
    public async prepareRates(): Promise<void> {
        const request = await this.httpService.get('https://api.exchangeratesapi.io/latest').toPromise();
        this.rates = request.data.rates;;

    }

    public calculateProductPriceByCurrency(product: Product | CreateProductDto, exchangeCurrency: string): number {

        const parseToDemandCurrency = (total: number, productCurrency: string) => {
            if (productCurrency === exchangeCurrency)
                return total;
            if (productCurrency === this.baseExchangeCurrency)
                return total * this.rates[exchangeCurrency];
            if (exchangeCurrency === this.baseExchangeCurrency)
                return total / this.rates[productCurrency];
            return (total / this.rates[productCurrency] * this.rates[exchangeCurrency]);
        };

        const {price, quantity, currency} = product;

        return +parseToDemandCurrency(price * quantity, currency).toFixed(2);
    }

    public async calculateCartTotalPriceByCurrency(cart: Cart | CreateCartDto, exchangeCurrency: string): Promise<number> {
        await this.prepareRates();

        const products: (Product | CreateProductDto)[] = cart.products;

        return +products
            .map((p: Product | CreateProductDto) => this.calculateProductPriceByCurrency(p, exchangeCurrency))
            .reduce((prev: number, next: number) => prev + next).toFixed(2);
    }
}
