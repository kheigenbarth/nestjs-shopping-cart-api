import { Test, TestingModule } from '@nestjs/testing';
import { ExchangeRatesService } from './exchange-rates.service';
import {HttpService} from "@nestjs/common";
import {ratesMock} from "./mocks/rates.mock";
import {HttpServiceMock} from "./mocks/http-service.mock";
import {CreateCartDto} from "../cart/dto/create-cart.dto";

describe('ExchangeRatesService', () => {
    let service: ExchangeRatesService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                ExchangeRatesService,
                {
                    provide: HttpService,
                    useClass: HttpServiceMock
                },
            ],
        }).compile();

        service = module.get<ExchangeRatesService>(ExchangeRatesService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });

    it('should exchange the currency on the selected currency (rates from redisService)', async () => {

        await service.prepareRates();

        const product1 = {"currency": "PLN", "description": "desc1", "name": "1", "price": 50.00, "quantity": 3};
        const product2 = {"currency": "EUR", "description": "desc2", "name": "2", "price": 25.50, "quantity": 5};
        const product3 = {"currency": "GBP", "description": "desc3", "name": "3", "price": 44.55, "quantity": 7};
        const product4 = {"currency": "GBP", "description": "desc4", "name": "4", "price": 78.55, "quantity": 9};

        let changedValue = service.calculateProductPriceByCurrency(product1, "EUR");
        let expectedValue = product1.quantity * product1.price / ratesMock[product1.currency];
        expect(changedValue).toEqual(+expectedValue.toFixed(2));

        changedValue = service.calculateProductPriceByCurrency(product2, "GBP");
        expectedValue = product2.quantity * product2.price * ratesMock["GBP"];
        expect(changedValue).toEqual(+expectedValue.toFixed(2));

        changedValue = service.calculateProductPriceByCurrency(product3, "PLN");
        expectedValue = product3.quantity * product3.price / ratesMock[product3.currency] * ratesMock["PLN"];
        expect(changedValue).toEqual(+expectedValue.toFixed(2));

        changedValue = service.calculateProductPriceByCurrency(product4, "GBP");
        expectedValue = product4.quantity * product4.price;
        expect(changedValue).toEqual(+expectedValue.toFixed(2));
    });

    it('should exchange the currency on the selected currency (rates from httpService)', async () => {
        await service.prepareRates();

        const product1 = {"currency": "PLN", "description": "desc1", "name": "1", "price": 50.00, "quantity": 3};
        const product2 = {"currency": "EUR", "description": "desc2", "name": "2", "price": 25.50, "quantity": 5};
        const product3 = {"currency": "GBP", "description": "desc3", "name": "3", "price": 44.55, "quantity": 7};

        let changedValue = service.calculateProductPriceByCurrency(product1, "EUR");
        let expectedValue = product1.quantity * product1.price / ratesMock[product1.currency];
        expect(changedValue).toEqual(+expectedValue.toFixed(2));

        changedValue = service.calculateProductPriceByCurrency(product2, "GBP");
        expectedValue = product2.quantity * product2.price * ratesMock["GBP"];
        expect(changedValue).toEqual(+expectedValue.toFixed(2));

        changedValue = service.calculateProductPriceByCurrency(product3, "PLN");
        expectedValue = product3.quantity * product3.price / ratesMock[product3.currency] * ratesMock["PLN"];
        expect(changedValue).toEqual(+expectedValue.toFixed(2));
    });

    it('should exchange the currency all products in the cart', async () => {
        await service.prepareRates();

        const product1 = {"currency": "PLN", "description": "desc1", "name": "1", "price": 50.00, "quantity": 3};
        const product2 = {"currency": "EUR", "description": "desc2", "name": "2", "price": 25.50, "quantity": 5};
        const product3 = {"currency": "GBP", "description": "desc3", "name": "3", "price": 44.55, "quantity": 7};

        const products = [product1, product2, product3];

        const product1ExpectedValue = product1.quantity * product1.price;
        const product2ExpectedValue = product2.quantity * product2.price * ratesMock["PLN"];
        const product3ExpectedValue = product3.quantity * product3.price / ratesMock['GBP'] * ratesMock["PLN"];

        const expectedValue = +product1ExpectedValue.toFixed(2) +
            +product2ExpectedValue.toFixed(2) +
            +product3ExpectedValue.toFixed(2);

        const cart: CreateCartDto = {
            products
        };

        const total: number = await service.calculateCartTotalPriceByCurrency(cart, "PLN");

        expect(total).toEqual(expectedValue)

    });
});