import {ratesMock} from "./rates.mock";
import {Observable, Subscriber} from "rxjs";

export class HttpServiceMock {
    get() {
        return Observable.create((observer: Subscriber<any>) => {
            observer.next({
                data: {
                    rates: ratesMock
                }
            });
            observer.complete()
        });
    }
}