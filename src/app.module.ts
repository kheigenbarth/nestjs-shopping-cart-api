import {CacheInterceptor, CacheModule, Module} from '@nestjs/common';
import {CartModule} from "./cart/cart.module";
import {MongooseModule} from '@nestjs/mongoose';
import {ConfigModule} from "@nestjs/config";
import {APP_INTERCEPTOR} from "@nestjs/core";

@Module({
    imports: [
        ConfigModule.forRoot({isGlobal: true,}),
        CartModule,
        MongooseModule.forRoot(`mongodb://${process.env.DATABASE_CONTAINER_NAME}`,
            {
                useNewUrlParser: true, useUnifiedTopology: true
            }),
        CacheModule.register({
            ttl: 3600
        })
    ],
    controllers: [],
    providers: [{
        provide: APP_INTERCEPTOR,
        useClass: CacheInterceptor,
    },],
})
export class AppModule {
}
