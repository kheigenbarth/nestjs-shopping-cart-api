import {registerDecorator, ValidationArguments, ValidationOptions} from "class-validator";


export function IsCurrencyValue(validationOptions?: ValidationOptions) {
    return function (object: unknown, propertyName: string) {
        registerDecorator({
            name: "isCurrencyValue",
            async: true,
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            validator: {
                validate(value: any, args: ValidationArguments) {
                    const currencyRegex = new RegExp('^\\s*(?=.*[1-9])\\d*(?:\\.\\d{1,2})?\\s*$');
                    return (typeof value === 'number' && currencyRegex.test(value.toString()))
                }
            }
        });
    };
}