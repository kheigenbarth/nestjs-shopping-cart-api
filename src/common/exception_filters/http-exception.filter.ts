import {
    ExceptionFilter,
    Catch,
    ArgumentsHost,
    HttpException,
    HttpStatus, Logger,
} from '@nestjs/common';

@Catch()
export class HttpExceptionFilter implements ExceptionFilter {

    catch(exception: Error | HttpException, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();
        const request = ctx.getRequest();

        if(exception instanceof HttpException) {
            return response.status(exception.getStatus()).send(exception.getResponse());
        }

        const status = HttpStatus.INTERNAL_SERVER_ERROR;

        const errorResponse = {
            statusCode: status,
            timestamp: new Date().toISOString(),
            path: request.url,
        };

        response.status(status).send(errorResponse);

        Logger.error(
            `${request.method} ${request.url}`,
            exception.stack,
            'ExceptionFilter',
        );
    }
}