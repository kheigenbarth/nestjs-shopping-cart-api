import * as mongoose from 'mongoose';
import {MongoMemoryServer} from 'mongodb-memory-server'

class MongoDatabaseMock {

    private mongod;

    async connect() {
        this.mongod = new MongoMemoryServer();
        const uri = await this.mongod.getConnectionString();

        const mongooseOpts = {
            useNewUrlParser: true,
            useUnifiedTopology: true
        };

        await mongoose.connect(uri, mongooseOpts);
    }

    async closeDb() {
        await mongoose.connection.dropDatabase();
        await mongoose.connection.close();
        await this.mongod.stop();
    }

    async getUri() {
        return this.mongod.getConnectionString();
    }
}

export default new MongoDatabaseMock()