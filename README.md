## Running the app

```bash
$ docker-compose up --build --d
```

## Documentation

http://localhost:2077:/docs

## Test

```bash
# node modules
$ npm i

# unit tests
$ npm run test

# test coverage
$ npm run test:cov
```